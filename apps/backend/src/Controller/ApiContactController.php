<?php

namespace App\Controller;

use App\Entity\Membership;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApiContactController extends AbstractController
{
    /**
     * @Route("/create-contact", name="create_contact")
     */
    public function createContact(
        Request $request,
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer,
    ): JsonResponse {
        $client = new CurlHttpClient();

        $person = new Person();
        $person->setFirstName('Chris');
        $person->setLastName('Ro');
        $person->setBirthDate(new \DateTime());
        $person->setCity('Paris');
        $person->setStreet('Rue du bois');
        $person->setZipCode(75000);
        $person->setMemberShip($entityManager->getRepository(Membership::class)->find(2));
        $person->setGender('M');
        $person->setEmail('chris@gmail.com');
        $person->setPhoneNumber('06000000000');
        $person->isActiveMember(true);

        $jsonContent = $serializer->serialize($person, 'json');

        $response = $client->request('POST', 'https://localhost:8000', [

        ]);

        $jsonResponse = new JsonResponse();
        $jsonResponse->setContent($jsonContent);
        $jsonResponse->setStatusCode($response->getStatusCode());


        return $jsonResponse;
    }

    /**
     * @Route("/api-create-contact", name="api_create_contact")
     */
    public function registerContact(Request $request)
    {
        // Logique de création & vérification des données + check isActiveMember
    }
}
