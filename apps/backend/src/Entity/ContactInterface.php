<?php

namespace App\Entity;

interface ContactInterface
{
    public function getEmail(): ?string;

    public function getStreet(): ?string;

    public function getZipCode(): ?int;

    public function getCity(): ?string;

    public function getMembership(): ?Membership;
}
