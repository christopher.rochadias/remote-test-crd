<?php

namespace App\DataFixtures;

use App\Entity\Structure;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StructureFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $structure = new Structure();
        $structure->setEmail('bob@gmail.com');
        $structure->setName('Bobs structure');
        $structure->setCity('Paris');
        $structure->setStreet('Rue du bois');
        $structure->setZipCode(75000);
        $structure->setMemberShip($this->getReference(MemberShipFixtures::MEMBERSHIP_REFERENCE));

        $manager->persist($structure);
        $manager->flush();
    }

    public function getDependencies()
    {
        return [MemberShipFixtures::class];
    }
}
