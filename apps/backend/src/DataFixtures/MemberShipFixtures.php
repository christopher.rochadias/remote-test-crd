<?php

namespace App\DataFixtures;

use App\Entity\Membership;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MemberShipFixtures extends Fixture
{
    public const MEMBERSHIP_REFERENCE = 'membership';

    public function load(ObjectManager $manager): void
    {
        $memberShip = new Membership();
        $memberShip->setType('non-profit');

        $this->setReference(self::MEMBERSHIP_REFERENCE, $memberShip);

        $manager->persist($memberShip);

        $manager->flush();
    }
}
