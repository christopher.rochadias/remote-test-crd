<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PersonFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $person = new Person();

        $person->setFirstName('Chris');
        $person->setLastName('Ro');
        $person->setBirthDate(new \DateTime());
        $person->setCity('Paris');
        $person->setStreet('Rue du bois');
        $person->setZipCode(75000);
        $person->setMemberShip($this->getReference(MemberShipFixtures::MEMBERSHIP_REFERENCE));

        $person->setGender('M');
        $person->setEmail('chris@gmail.com');
        $person->setPhoneNumber('06000000000');

        $manager->persist($person);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [MemberShipFixtures::class];
    }
}
